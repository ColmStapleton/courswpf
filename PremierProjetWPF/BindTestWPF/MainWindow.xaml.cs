﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BindTestWPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Small(object sender, RoutedEventArgs e)
        {
            lblSampleText.FontSize = 5;
        }

        private void Normal(object sender, RoutedEventArgs e)
        {
            lblSampleText.FontSize = 15;
        }

        private void Large(object sender, RoutedEventArgs e)
        {
            lblSampleText.FontSize = 35;
        }
    }
}
