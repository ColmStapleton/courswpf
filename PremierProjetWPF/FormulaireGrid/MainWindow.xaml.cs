﻿using FormulaireGrid.ViewModels;
using FormulaireGrid.Views;
using PremierProjetWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FormulaireGrid
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public Boolean IsModif = false;
        public Personne pModif;
        public MainWindow()
        {
            InitializeComponent();

            //DataContext = this;      
        }

        private void OK(object sender, RoutedEventArgs e)
        {
            string nom = "";
            string prenom = "";
            int age = 0;
            string description = "";
            Boolean valid = true;

            if (champNom.Text.Length > 0)
            {
                nom = champNom.Text;
            }
            else
            {
                Empty(champNom);
                valid = false;
                MessageBox.Show("Nom vide");
            }

            if (champPrenom.Text.Length > 0)
            {
                prenom = champPrenom.Text;
            }
            else
            {
                Empty(champPrenom);
                valid = false;
                MessageBox.Show("Prénom vide");
            }

            if (champAge.Text.Length > 0)
            {
                age = int.Parse(champAge.Text);
            }
            else
            {
                Empty(champAge);
                valid = false;
            }

            if (champDescription.Text.Length > 0)
            {
                description = champDescription.Text;
            }
            else
            {
                Empty(champDescription);
                valid = false;
                MessageBox.Show("Description vide");
            }

            /*

            if (valid == true && IsModif == false)
            {
                Personne p = new Personne(nom, prenom, age, description);
                Personnes.Add(p);

                Empty(champAge);
                Empty(champDescription);
                Empty(champNom);
                Empty(champPrenom);
            }
            else if (valid == true && IsModif == true)
            {

                Personne p = Personnes.FirstOrDefault(person => person.Id == pModif.Id);

                p.Age = age;
                p.Description = description;
                p.Nom = nom;
                p.Prenom = prenom;

                Empty(champAge);
                Empty(champDescription);
                Empty(champNom);
                Empty(champPrenom);

                IsModif = false;
            }

            */
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Empty(champAge);
            Empty(champDescription);
            Empty(champNom);
            Empty(champPrenom);
        }

        private void Empty(TextBox champ)
        {
            champ.Text = "";
            //MessageBox.Show("Opération invalide");
        }

        private void Nombres(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Search(object sender, RoutedEventArgs e)
        {
            /*
            int id;
            if (int.TryParse(idSelector.Text, out id))
            {
                if (Personnes.FirstOrDefault(person => person.Id == id) is null)
                {
                    MessageBox.Show("Cette personne n'existe pas");
                }
                else
                {
                    Personne p = Personnes.FirstOrDefault(person => person.Id == id);
                    champNom.Text = p.Nom;
                    champPrenom.Text = p.Prenom;
                    champAge.Text = p.Age.ToString();
                    champDescription.Text = p.Description;
                }
            }
            else
            {
                idSelector.Text = "";
            }
            */
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            /*
            Button myButton = (Button)sender;
            object myObject = myButton.DataContext;
            Personne p = (Personne)myObject;
            Personnes.Remove(p);
            */
        }

        private void Modifier(object sender, RoutedEventArgs e)
        {
            Button myButton = (Button)sender;
            object myObject = myButton.DataContext;
            pModif = (Personne)myObject;

            champNom.Text = pModif.Nom;
            champPrenom.Text = pModif.Prenom;
            champAge.Text = pModif.Age.ToString();
            champDescription.Text = pModif.Description;

            IsModif = true;
        }

        private void OpenList(object sender, RoutedEventArgs e)
        {
            ListeView nEcranListe = new ListeView();
            nEcranListe.Show();
        }
    }
}
