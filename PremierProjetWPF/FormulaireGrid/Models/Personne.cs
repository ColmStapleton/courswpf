﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremierProjetWPF.Models
{

    public class Personne
    {
        private static int idCount = 0;

        private int _id;
        private string _nom;
        private string _prenom;
        private int _age;
        private string _description;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
        public string Prenom
        {
            get { return _prenom; }
            set { _prenom = value; }
        }

        public string Description { get => _description; set => _description=value; }

        public int Id { get => _id; set => _id=value; }

        public Personne(string n, string p, int age, string desc)
        {
            this.Nom = n;
            this.Prenom = p;
            this.Age = age;
            this.Description = desc;
            this.Id = idCount++;
        }

        public Personne()
        {

        }

    }
}
