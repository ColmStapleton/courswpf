﻿using PremierProjetWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaireGrid.ViewModels
{
    public class ListeViewViewModel : BaseViewModel
    {
        public ObservableCollection<Personne> Personnes { get; set; }

        public ListeViewViewModel()
        {
            FetchListPersons();
        }

        public void FetchListPersons()
        {
            ObservableCollection<Personne> personnes = new ObservableCollection<Personne>();

            personnes.Add(new Personne("Stapleton", "Colm", 30, "Description"));
            personnes.Add(new Personne("Schmitt", "Jean-Paul", 40, "Description"));

            Personnes = personnes;

        } 
    }
}
