﻿using PremierProjetWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PremierProjetWPF.DataContext
{
    /// <summary>
    /// Logique d'interaction pour TPDataContext.xaml
    /// </summary>
    public partial class TPDataContext : Window
    {
        public TPDataContext()
        {
            InitializeComponent();

            Personne p1 = new Personne("Durand","Pierre",2);
            //secondPerson.DataContext = p1;
            DataContext = p1;
        }
    }
}
