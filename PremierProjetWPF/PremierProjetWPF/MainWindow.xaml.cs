﻿using PremierProjetWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PremierProjetWPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new EcranMVVM_ViewModel();

            /*
            #region Initialisation des Items du ListBox1
            ListBox listBox1 = new ListBox();
            listBox1.SelectedIndex = 0;
            listBox1.Background = Brushes.Blue;


            TextBlock textBlock1 = new TextBlock();
            textBlock1.Text = "Hello!";
            listBox1.Items.Add(textBlock1);

            TextBlock textBlock2 = new TextBlock();
            textBlock2.Text = "Thank You!";
            listBox1.Items.Add(textBlock2);

            TextBlock textBlock3 = new TextBlock();
            textBlock3.Text = "Goodbye!";
            listBox1.Items.Add(textBlock3);

            listBox1.Items.Add(new TextBlock()
                {
                    Text = "See You Soon!"
                }
            );

            //Grid.SetRow(listBox1, 0);
            //myGrid.Children.Add(listBox1);
            //Content = listBox1;

            #endregion
            */

        }

        private void Sauvegarder(object sender, RoutedEventArgs e)
        {
            EcranMVVM_ViewModel vm = ((EcranMVVM_ViewModel)this.DataContext);
            vm.Age = 45;
            vm.Nom = "Toto";
            vm.Prenom = "Titi";

            Master nEcranMaster = new Master((EcranMVVM_ViewModel)this.DataContext);
            nEcranMaster.Show();
        }
    }
}
