﻿using PremierProjetWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PremierProjetWPF
{
    /// <summary>
    /// Logique d'interaction pour Master.xaml
    /// </summary>
    public partial class Master : Window
    {
        public Master(EcranMVVM_ViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }
    }
}
