﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremierProjetWPF.Models
{
    public class Personne
    {
        private static int countId = 0;
        private string nom;
        private int id;
        private string prenom;

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public Personne(string n, string p, int id)
        {
            this.Nom = n;
            this.Prenom = p;
            this.Id = id;
        }

        public Personne()
        {

        }

    }
}
