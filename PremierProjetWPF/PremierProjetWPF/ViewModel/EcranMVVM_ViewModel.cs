﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremierProjetWPF.ViewModel
{
    public class EcranMVVM_ViewModel : BaseViewModel
    {
        private string _nom;
        private string _prenom;
        private int _age;

        public string Nom
        {
            get { return _nom; }
            set
            {
                if (_nom != value)
                {
                    _nom = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Prenom
        {
            get { return _prenom; }
            set
            {
                if (_prenom != value)
                {
                    _prenom = value;
                    OnPropertyChanged();
                }
            }
        }

        public int Age
        {
            get { return _age; }
            set
            {
                if (_age != value)
                {
                    _age = value;
                }
            }
        }

        public EcranMVVM_ViewModel()
        {
            Nom = "STAPLETON";
            Prenom = "Colm";
            Age = 30;
        }
    }
}
